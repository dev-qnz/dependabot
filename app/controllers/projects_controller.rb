# frozen_string_literal: true

class ProjectsController < ApplicationController
  before_action :authenticate_user!

  # Render index projects page
  #
  # @return [void]
  def index
    @projects = Project.not(configuration: nil).order(name: :asc)
  end

  # Add new project
  #
  # @return [void]
  def create
    project_name = params.require(:project_name)
    access_token = params[:access_token].presence

    existing_project = Project.where(name: project_name).first
    return redirect_to(projects_path, notice: "Project already added!") if existing_project

    project = Dependabot::Projects::Creator.call(params.require(:project_name), access_token: access_token)
    return redirect_to(projects_path, notice: "No configuration in the project") unless project.configuration

    Cron::JobSync.call(project)
    redirect_to projects_path, notice: "Project added"
  rescue Gitlab::Error::Error => e
    redirect_to projects_path, alert: e.message
  end

  # Sync existing project
  #
  # @return [void]
  def update
    project = Project.find_by(id: params.require(:id))

    Dependabot::Projects::Creator.call(project.name, access_token: project.gitlab_access_token)
    Cron::JobSync.call(project)

    redirect_to projects_path
  end
end
