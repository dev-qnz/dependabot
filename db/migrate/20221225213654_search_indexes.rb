# frozen_string_literal: true

class SearchIndexes < Mongoid::Migration
  def self.up
    Project.create_indexes
    MergeRequest.create_indexes
    User.create_indexes
    Vulnerability.create_indexes
    VulnerabilityIssue.create_indexes
  end

  def self.down
    Project.remove_indexes
    MergeRequest.remove_indexes
    User.remove_indexes
    Vulnerability.remove_indexes
    VulnerabilityIssue.remove_indexes
  end
end
