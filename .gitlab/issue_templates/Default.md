# 🐞 Bug report

The more information you can provide, the easier it will be to reproduce the issue and find a fix

## Is there an existing issue for this?

Please search existing issues to avoid creating duplicates

- [ ] I have searched the existing issues

## App version

<!-- Application version used -->

## Execution mode

<!-- Execution mode of the application: deployed or standalone -->

## Package ecosystem

<!-- Please state ecosystem you are using like npm, Docker, Bundler, etc. -->

## Package manager version

<!-- If applicable, specify the package manager version you're using (e.g., npm `7.1`, pip-compile `5.0`, etc.) -->

## Language version

<!-- If applicable, specify the language version you're using (e.g., Node.js `14.1`, Ruby `3.1`, etc.) -->

## dependabot.yml

<!-- Add your `dependabot.yml` file or provide a link to it -->

```yml
```

## Updated dependency

<!-- If applicable, the dependency name and to and from versions -->

## Expected outcome

<!--  A clear and concise description of what you expected to happen -->

## Native package manager behaviour

<!-- If applicable, what output do you see when you update the dependency using the native package manager (e.g., Bundler, npm, etc.)? -->

## Log output

<!-- If applicable, paste in any related logs, preferably with debug output turned on -->

```console
```

## Smallest manifest that reproduces the issue

<!-- If possible, small project or manifest file that can reproduce the problem -->
